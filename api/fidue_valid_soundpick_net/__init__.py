from __future__ import absolute_import

import os

from flask import Flask, send_from_directory, session, request, render_template, g
from flask_session import Session
from flask_bcrypt import Bcrypt

from .config import database
from datetime import timedelta

app = Flask(__name__, template_folder='template', static_url_path='/static')
app.secret_key = 'valid.fiduekorea.com'

app.config.from_object(database)
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()
db.init_app(app)

@app.route('/static/<path:path>')
def send_js(path):
	return send_from_directory('static/', path)

@app.after_request
def add_header(response):
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response

from .controller.product import product
from .controller.valid import valid
from .controller.swap import swap
from .controller.reuse import reuse

app.register_blueprint(product, url_prefix='/api/product')
app.register_blueprint(valid, url_prefix='/api/valid')
app.register_blueprint(swap, url_prefix='/api/swap')
app.register_blueprint(reuse, url_prefix='/api/reuse')