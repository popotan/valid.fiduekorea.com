from flask import Blueprint, render_template, jsonify, request, session, redirect
from sqlalchemy import desc, asc

from fidue_valid_soundpick_net import app
from datetime import datetime, timedelta

from fidue_valid_soundpick_net.model.product import product_orm
from fidue_valid_soundpick_net.model.product_key import product_key_orm
from fidue_valid_soundpick_net.model.buy_user import buy_user_orm

product = Blueprint('product', __name__)

@product.route('/key_startswitch', methods=['GET'])
def key_startswitch_list():
    product_list = product_orm().query.all()
    return jsonify({
        'message' : 'SUCCESS',
        'response' : [row.key_startswitch for row in product_list]
    })

@product.route('/regist', methods=['POST'])
def product_regist():
    data = request.get_json()
    serial, user_name, user_phone = data['serial'], data['userName'], data['userPhone']

    pkey = product_key_orm().query.filter_by(serial=serial).first()
    if pkey:
        is_buy_user = buy_user_orm().query.filter_by(serial=serial).first()
        if is_buy_user:
            return jsonify({
                'message' : 'ALREADY_EXIST_THAT_PRODUCT_KEY_HAD'
            })
        else:
            new_user = buy_user_orm()
            new_user.serial = serial
            new_user.user_name = user_name
            new_user.user_phone = user_phone
            new_user.receipt_image_filename = data['imageFileName']
            new_user.set_buy_date(year=data['year'],month=data['month'],date=data['day'])
            new_user.add()
            if new_user.id:
                return jsonify({
                    'message' : 'SUCCESS'
                })
            else:
                return jsonify({
                    'message' : 'USER_INFO_IS_NOT_VALID'
                })
    else:
        return jsonify({
            'message' : 'PRODUCT_KEY_IS_NOT_EXIST'
        })

@product.route('/serial/<serial>', methods=['GET'])
def serial_info(serial):
    product = product_key_orm().query.filter_by(serial=serial).first()
    if product:
        is_buy_user = buy_user_orm().query.filter_by(serial=serial).first()
        if is_buy_user:
            repr_cols = ['id', 'serial', 'product', 'reg_date']
            return jsonify({
                'message' : 'ALREADY_EXIST_THAT_PRODUCT_KEY_HAD',
                'response' : {_col : getattr(product, _col) for _col in repr_cols}
            })
        else:
            repr_cols = ['id', 'serial', 'product', 'reg_date']
            return jsonify({
                'message' : 'SUCCESS',
                'response' : {_col : getattr(product, _col) for _col in repr_cols}
            })
    else:
        return jsonify({
            'message' : 'PRODUCT_KEY_IS_NOT_EXIST'
        })