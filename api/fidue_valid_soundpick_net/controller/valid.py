#-*- coding: utf-8 -*-
from flask import Blueprint, render_template, jsonify, request, session, redirect
from sqlalchemy import desc, asc

from fidue_valid_soundpick_net import app
from datetime import datetime, timedelta

from fidue_valid_soundpick_net.model.product import product_orm
from fidue_valid_soundpick_net.model.product_key import product_key_orm
from fidue_valid_soundpick_net.model.buy_user import buy_user_orm

from fidue_valid_soundpick_net.config import static as STATIC

valid = Blueprint('valid', __name__)

@valid.route('/serial/<serial>', methods=['GET'])
def get_serial_validation(serial):
    if request.method == 'GET':
        pkey = product_key_orm().query.filter_by(serial=serial).first()
        if pkey:
            if pkey.user:
                repr_cols = ['id', 'serial', 'product', 'user', 'reg_date']
                return jsonify({
                    'message' : 'SUCCESS',
                    'response' : {_col : getattr(pkey, _col) for _col in repr_cols}
                })
            else:
                return jsonify({
                    'message' : 'SERIAL_OWNER_IS_NOT_EXIST'
                })
        else:
            return jsonify({
                'message' : 'PRODUCT_KEY_IS_NOT_EXIST'
            })

@valid.route('/receipt', methods=['POST'])
def receipt():
    if request.method == 'POST':
        filename = upload()
        if filename:
            return jsonify({
                'status' : 201,
                'message' : 'SUCCESS',
                'response' : filename
            }), 200
        else:
            return jsonify({
                'status' : 204,
                'message' : 'FAIL'
            }), 200

def upload():
    def allowed_file(filename):
	    ALLOWED_EXTENSIONS = set(['png', 'jpeg', 'jpg', 'gif', 'PNG', 'JPEG', 'JPG', 'GIF'])
	    return '.' in filename and \
			filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS
        
    file = request.files['upload_file']
    if file and allowed_file(file.filename):
        now = datetime.utcnow() + timedelta(hours=9)
            
        filename = str(now.year) + str(now.month) + str(now.day) + str(now.hour) + str(now.minute) + str(now.second)
        filename += '_' + file.filename.replace(' ', '')
        filename = filename.encode('ascii', errors='ignore').decode()
        file.save(STATIC.FILE_PATH.format('fidue/valid/receipt', filename))
        return filename
    else:
        return ''

@valid.route('/valid/serial/<serial>/mod/image_filename', methods=['POST'])
def mod_image_filename(serial):
    new_image_filename = request.get_json()['new_filename']
    info = buy_user_orm().query.filter_by(serial=serial).first()
    if info:
        if info.mod_valid:
            info.image_filename = new_image_filename
            info.update()
            repr_cols = ['id', 'serial', 'mod_valid', 'user_name', 'user_phone', 'receipt_image_filename', 'buy_date', 'reg_date']
            return jsonify({
                'message' : 'SUCCESS',
                'response' : {_col : getattr(info, _col) for _col in repr_cols}
            })
        else:
            return jsonify({
                'message' : 'MODIFY_IS_NOT_AVAILABLE'
            })
    else:
        return jsonify({
            'message' : 'SERIAL_INFO_IS_NOT_EXIST'
        })