from flask import Blueprint, render_template, jsonify, request, session, redirect
from sqlalchemy import desc, asc

from fidue_valid_soundpick_net import app
from datetime import datetime, timedelta

import random

from fidue_valid_soundpick_net.model.product import product_orm
from fidue_valid_soundpick_net.model.product_key import product_key_orm
from fidue_valid_soundpick_net.model.buy_user import buy_user_orm
from fidue_valid_soundpick_net.model.case_history import case_history_orm
from fidue_valid_soundpick_net.model.reuse_accept_log import reuse_accept_log_orm

from fidue_valid_soundpick_net.composer.sms import Sms

reuse = Blueprint('reuse', __name__)

@reuse.route('/target/<serial>', methods=['GET'])
def get_serial_origin_user(serial):
    info = buy_user_orm().query.filter_by(serial=serial).first()
    if info:
        repr_cols = ['id', 'serial', 'product', 'reg_date']
        return jsonify({
            'message' : 'SUCCESS',
            'response' : {_col : getattr(info, _col) for _col in repr_cols}
        })
    else:
        return jsonify({
            'message' : 'IS_NOT_EXIST_PRODUCT_KEY_TOKENER'
        })

@reuse.route('/<int:reuse_accept_log_id>', methods=['GET'])
def get_reuse_accept_log_info(reuse_accept_log_id):
    accept_log = reuse_accept_log_orm().query.filter_by(id=reuse_accept_log_id, is_accepted=None).first()
    if accept_log:
        repr_cols = ['buy_user', 'user_name', 'user_phone', 'reg_date']
        return jsonify({
            'message' : 'SUCCESS',
            'response' : {_col : getattr(accept_log, _col) for _col in repr_cols}
        })
    else:
        return jsonify({'message' : 'REUSE_REQUEST_IS_NOT_EXIST'})

@reuse.route('/request/<int:buy_user_id>', methods=['POST'])
def request_reuse_to_origin(buy_user_id):
    origin = buy_user_orm().query.filter_by(id=buy_user_id).first()
    if origin and origin.serial == request.get_json()['serial']:
        accept_log = reuse_accept_log_orm().query.filter_by(buy_user_id=buy_user_id, is_accepted=None).all()
        if accept_log:
            return jsonify({'message' : 'ALREADY_REQUESTED'})
        else:
            accept_log = reuse_accept_log_orm()
            accept_log.buy_user_id = buy_user_id
            accept_log.valid_number = str(random.randrange(1000, 10000))
            accept_log.user_name = request.get_json()['user_name']
            accept_log.user_phone = request.get_json()['user_phone']
            accept_log.add()
            #TODO : SMS 전송하기
            Sms(
                title='피듀코리아 제품 사용자 이전 승인 안내',
                to_number=origin.user_phone.replace('-',''),
                from_number='07078140007',
                desc='안녕하세요. 피듀코리아입니다.\n' + accept_log.user_name + '님으로부터 제품번호 ' + origin.serial + '의 소유권 이전 요청이 접수되었습니다.\n아래의 링크에 접속하여, 소유권 이전 승인 또는 거절을 선택하여 주시기 바랍니다.\n인증번호는 ' + str(accept_log.valid_number) + '입니다.\n승인전용주소 : http://valid.fiduekorea.com/reuse/accept/' + str(accept_log.id)
                ).send()
            return jsonify({'message' : 'SUCCESS'})
    else:
        return jsonify({'message' : 'USER_INFO_IS_NOT_MATCHED'})

@reuse.route('/accept/<int:accept_id>', methods=['POST'])
def update_buy_user(accept_id):
    accept_log = reuse_accept_log_orm().query.filter_by(id=accept_id, is_accepted=None).first()
    if accept_log:
        if accept_log.valid_number == request.get_json()['valid_number']:
            if request.get_json()['is_accepted']:
                origin = buy_user_orm().query.filter_by(id=accept_log.buy_user_id).first()
                allow_mod_cols = ['user_name', 'user_phone']
                save_hist_and_origin_change(origin, allow_mod_cols, accept_log)
                accept_log.is_accepted = True
                accept_log.accept_date = datetime.utcnow() + timedelta(hours=9)
                accept_log.update()
                return jsonify({'message' : 'SUCCESS'})
            else:
                accept_log.is_accepted = False
                accept_log.accept_date = datetime.utcnow() + timedelta(hours=9)
                accept_log.update()
                return jsonify({'message' : 'ACCEPT_REQUEST_IS_SHUTDOWN'})
        else:
            return jsonify({'message' : 'VALID_NUMBER_IS_NOT_MATCHED'})
    else:
        return jsonify({'message' : 'IS_NOT_EXIST_REUSE_REQUEST'})
    

def save_hist_and_origin_change(origin, allow_mod_cols, data):
    hist = case_history_orm()
    for col in allow_mod_cols:
        if hasattr(data, col):
            setattr(hist, col, getattr(origin, col))
            setattr(origin, col, getattr(data, col))
    if hasattr(data, 'comment'):
        hist.comment = data['comment']
    hist.buy_user_id = origin.id
    hist.add()
    origin.update()