from flask import Blueprint, render_template, jsonify, request, session, redirect
from sqlalchemy import desc, asc

from fidue_valid_soundpick_net import app
from datetime import datetime, timedelta

from fidue_valid_soundpick_net.model.product import product_orm
from fidue_valid_soundpick_net.model.product_key import product_key_orm
from fidue_valid_soundpick_net.model.buy_user import buy_user_orm
from fidue_valid_soundpick_net.model.case_history import case_history_orm

swap = Blueprint('swap', __name__)

@swap.route('/target', methods=['POST'])
def get_swap_target_list():
    my_list = buy_user_orm().query.filter_by(user_name=request.get_json()['user_name'],user_phone=request.get_json()['user_phone']).all()
    repr_cols = ['id', 'serial', 'buy_date', 'reg_date', 'product']
    return jsonify({
        'message' : 'SUCCESS',
        'response' : [{_col : getattr(_row, _col) for _col in repr_cols} for _row in my_list]
    })

@swap.route('/<int:buy_user_id>', methods=['PUT'])
def update_buy_user(buy_user_id):
    origin = buy_user_orm().query.filter_by(id=buy_user_id).first()
    allow_mod_cols = ['serial']
    data = request.get_json()
    if origin.user_name == data['user_name'] and origin.user_phone == data['user_phone']:
        save_hist_and_origin_change(origin, allow_mod_cols, data)
        return jsonify({
            'message' : 'SUCCESS'
        })
    else:
        return jsonify({
            'message' : 'USER_INFO_IS_NOT_MATCHED'
        })

def save_hist_and_origin_change(origin, allow_mod_cols, data):
    hist = case_history_orm()
    for col in allow_mod_cols:
        if col in data:
            setattr(hist, col, getattr(origin, col))
            setattr(origin, col, data[col])
    if 'comment' in data:
        hist.comment = data['comment']
    hist.buy_user_id = origin.id
    hist.add()
    origin.update()