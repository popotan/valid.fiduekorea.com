from openpyxl import Workbook
from openpyxl import load_workbook

class SerialFileParser(object):

    def __init__(self, filename):
        self.FILENAME = filename
        self.workbook = load_work(filename=self.FILENAME)
        self.workbook.active
        pass

    def get_sheet_names(self):
        return self.workbook.get_sheet_names()

    def set_active_sheet(self, sheet_name):
        self.active_sheet = self.workbook[sheet_name]
        return self

    def get_column_names(self, sheet_name):
        temp_sheet = self.workbook[sheet_name]
        result = []
        for title in temp_sheet[0]:
            result.append(title.value)
        return result

    def get_selected_row(self, column_num):
        column_alpha = chr(97 + column_num)
        result = []
        for idx, col in self.active_sheet:
            result.append(col[column_alpha + str(idx+1)])
        return result