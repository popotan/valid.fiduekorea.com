# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey
# from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from fidue_valid_soundpick_net import db

from datetime import datetime, timedelta

class reuse_accept_log_orm(db.Model):
    __bind_key__ = 'fidue'
    __tablename__ = 'reuse_accept_log'
    id = Column(Integer,primary_key=True,unique=True)
    valid_number = Column(String)
    buy_user_id = Column(Integer)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))
    accept_date = Column(DateTime,nullable=True)
    is_accepted = Column(Boolean,nullable=True)
    user_name = Column(String,nullable=True)
    user_phone = Column(String,nullable=True)

    @property
    def buy_user(self):
        from .buy_user import buy_user_orm
        if self.buy_user_id:
            user = buy_user_orm().query.filter_by(id=self.buy_user_id).first()
            repr_cols = ['id', 'serial', 'user_name', 'user_phone', 'buy_date', 'reg_date', 'product']
            return {_col : getattr(user, _col) for _col in repr_cols}
        else:
            return {}

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        self.last_updated = datetime.utcnow()+timedelta(hours=9)
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()