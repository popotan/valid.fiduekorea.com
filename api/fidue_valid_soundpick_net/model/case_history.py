# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey
# from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from fidue_valid_soundpick_net import db

from datetime import datetime, timedelta

class case_history_orm(db.Model):
    __bind_key__ = 'fidue'
    __tablename__ = 'case_history'
    id = Column(Integer,primary_key=True,unique=True)
    buy_user_id = Column(Integer)
    serial = Column(String)
    user_name = Column(String)
    user_phone = Column(String)
    _buy_date = Column('buy_date', DateTime)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))
    comment = Column(String)

    @property
    def buy_date(self):
        obj = {
        "year" : self._buy_date.year,
        "month" : self._buy_date.month,
        "day" : self._buy_date.day }
        return obj

    def set_buy_date(self, year, month, date):
        self._buy_date = datetime(year, month, date)

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        self.last_updated = datetime.utcnow()+timedelta(hours=9)
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()