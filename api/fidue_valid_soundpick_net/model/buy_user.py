# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey
# from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from fidue_valid_soundpick_net import db

from datetime import datetime, timedelta

class buy_user_orm(db.Model):
    __bind_key__ = 'fidue'
    __tablename__ = 'buy_user'
    id = Column(Integer,primary_key=True,unique=True)
    serial = Column(String, unique=True)
    mod_valid = Column(Boolean, default=False)
    user_name = Column(String)
    user_phone = Column(String)
    receipt_image_filename = Column(String)
    _buy_date = Column('buy_date', DateTime)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    @property
    def product(self):
        from .product_key import product_key_orm
        if self.serial:
            product_key = product_key_orm().query.filter_by(serial=self.serial).first()
            return product_key.product
        else:
            return {}

    @property
    def buy_date(self):
        obj = {
        "year" : self._buy_date.year,
        "month" : self._buy_date.month,
        "day" : self._buy_date.day }
        return obj

    def set_buy_date(self, year, month, date):
        self._buy_date = datetime(year, month, date)

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        self.last_updated = datetime.utcnow()+timedelta(hours=9)
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()