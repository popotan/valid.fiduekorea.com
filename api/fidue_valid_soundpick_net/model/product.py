# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey
# from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from fidue_valid_soundpick_net import db

from datetime import datetime, timedelta

from .product_image import product_image_orm

class product_orm(db.Model):
    __bind_key__ = 'fidue'
    __tablename__ = 'product'
    id = Column(Integer,primary_key=True,unique=True)
    model = Column(String)
    key_startswitch = Column(String)
    warranty_day_length = Column(Integer)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    @property
    def product_image(self):
        images = product_image_orm().query.filter_by(product_id=self.id).all()
        repr_cols = ['id', 'image_filename', 'reg_date']
        return [{_col : getattr(_row, _col) for _col in repr_cols} for _row in images]

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        self.last_updated = datetime.utcnow()+timedelta(hours=9)
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()