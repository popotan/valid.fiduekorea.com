# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey
# from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from fidue_valid_soundpick_net import db

from datetime import datetime, timedelta

class product_key_orm(db.Model):
    __bind_key__ = 'fidue'
    __tablename__ = 'product_key'
    id = Column(Integer,primary_key=True,unique=True)
    product_id = Column(Integer)
    serial = Column(String, unique=True)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    @property
    def product(self):
        from .product import product_orm
        if self.product_id:
            p = product_orm().query.filter_by(id=self.product_id).first()
            repr_cols = ['id', 'model', 'key_startswitch', 'warranty_day_length', 'product_image', 'reg_date']
            return {_col : getattr(p, _col) for _col in repr_cols}
        else:
            return {}

    @property
    def user(self):
        from .buy_user import buy_user_orm
        user = buy_user_orm().query.filter_by(serial=self.serial).first()
        if user:
            repr_cols = ['id', 'mod_valid', 'user_name', 'user_phone', 'receipt_image_filename', 'buy_date', 'reg_date']
            return {_col : getattr(user, _col) for _col in repr_cols}
        else:
            return None

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        self.last_updated = datetime.utcnow()+timedelta(hours=9)
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()