import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { SharedModule } from './shared/shared.module';

import { AppComponent } from './app.component';
import { RegistComponent } from './regist/regist.component';
import { AscertainComponent } from './ascertain/ascertain.component';
import { IndexComponent } from './index/index.component';
import { ResultComponent } from './regist/result/result.component';
import { AscertainResultComponent } from './ascertain/ascertain-result/ascertain-result.component';

import { ComponentStateService } from './shared/service/component-state.service';
import { ReuseComponent } from './reuse/reuse.component';
import { SwapComponent } from './swap/swap.component';
import { AcceptComponent } from './reuse/accept/accept.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistComponent,
    AscertainComponent,
    IndexComponent,
    ResultComponent,
    AscertainResultComponent,
    ReuseComponent,
    SwapComponent,
    AcceptComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    SharedModule,
    RouterModule.forRoot([
      {
        path: '',
        component: IndexComponent
      }, {
        path: 'regist',
        component: RegistComponent
      }, {
        path: 'result',
        component: ResultComponent
      }, {
        path: 'ascertain',
        component: AscertainComponent
      }, {
        path: 'reuse',
        component: ReuseComponent
      }, {
        path : 'reuse/accept/:accept_id',
        component : AcceptComponent
      }, {
        path: 'swap',
        component: SwapComponent
      }
    ])
  ],
  providers: [ComponentStateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
