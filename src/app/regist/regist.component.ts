import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ComponentStateService } from '../shared/service/component-state.service';
import { FileUploadService } from '../shared/service/file-upload.service';
import { RegistService } from './regist.service';

import { apiURL } from '../../environments/environment.prod';

@Component({
  selector: 'app-regist',
  templateUrl: './regist.component.html',
  styleUrls: ['./regist.component.css'],
  providers: [FileUploadService, RegistService]
})
export class RegistComponent implements OnInit {

  /**
   * @description 구매증빙 업로드 예시 element
   * @memberof RegistComponent
   */
  @ViewChild('receiptguidance') receiptGuidance;

  /**
   * @description 시리얼 허용 문자열(startswitch) 리스트, onInit시, 서버로부터 갱신받아온다.
   * @memberof RegistComponent
   */
  key_startswitch_list = ['TXYA65', 'TXYA73', 'TXYA83', 'F0', 'FD00', 'GA'];

  /**
   * @description 제품정보 placeholder값 세팅을 위한 date객체
   * @memberof RegistComponent
   */
  today = new Date();

  /**
   * @description 등록정보
   * @memberof RegistComponent
   */
  productRegist = new FormGroup({
    clauseAgree: new FormControl('',
      Validators.compose([Validators.requiredTrue])),
    serial: new FormControl('',
      Validators.compose([Validators.required])),
    userName: new FormControl('',
      Validators.compose([Validators.required])),
    userPhone: new FormControl('',
      Validators.compose([Validators.required])),
    year: new FormControl(this.today.getFullYear(),
      Validators.compose([Validators.required])),
    month: new FormControl(this.today.getMonth() + 1,
      Validators.compose([Validators.required])),
    day: new FormControl(this.today.getDate(),
      Validators.compose([Validators.required])),
    imageFileName: new FormControl('',
      Validators.compose([Validators.required]))
  });

  /**
   * @description key_startswitch_list와 입력된 값 일치여부
   * @memberof RegistComponent
   */
  isKeyStartswitchNotMatched = false;

  /**
   * @description 
   * @memberof RegistComponent
   */
  product = null;

  /**
   * @description 이미 등록된 시리얼일 경우 경고창 노출 여부
   * @memberof RegistComponent
   */
  isAlreadyUserExist = false;

  /**
   * @description 존재하지 않는 시리얼일 경우 경고창 노출 여부
   * @memberof RegistComponent
   */
  isNotExistProductKey = false;

  /**
   * @description 구매증빙이미지 업로드 성공 여부
   * @memberof RegistComponent
   */
  isImageUploading = false;

  constructor(
    private cs: ComponentStateService,
    private fileUploadService: FileUploadService,
    private registService: RegistService,
    private router: Router
  ) { }

  /**
   * @description 시리얼 규칙(startswitch) 획득
   * @memberof RegistComponent
   */
  ngOnInit() {
    this.registService.getKeyStartswitchList()
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.key_startswitch_list = result['response'];
        }
      });
  }

  /**
   * @description 시리얼 규칙 매칭 여부 검사
   * @param {*} $event
   * @memberof RegistComponent
   */
  isKeyStartswitchMatched($event) {
    const serial = $event.target.value;
    const regexSerial = new RegExp('^' + serial);
    // tslint:disable-next-line:no-var-keyword
    for (var i = 0; i < this.key_startswitch_list.length; i++) {
      const regexKey = new RegExp('^' + this.key_startswitch_list[i]);
      if (
        regexSerial.test(this.key_startswitch_list[i]) ||
        regexKey.test(serial)
      ) {
        break;
      }
    }
    if (i >= this.key_startswitch_list.length) {
      this.isKeyStartswitchNotMatched = true;
    } else {
      this.isKeyStartswitchNotMatched = false;
    }
  }

  /**
   * @description 시리얼 검증
   * @param {*} $event
   * @memberof RegistComponent
   */
  validateSerial($event) {
    $event.preventDefault();
    this.isAlreadyUserExist = false;
    this.isNotExistProductKey = false;
    if (this.productRegist.get('clauseAgree').value) {
      const serial = this.productRegist.get('serial').value;
      this.registService.getSerialInfo(serial)
        .subscribe(result => {
          if (result['message'] === 'SUCCESS') {
            this.product = result['response'];
          } else if (result['message'] === 'ALREADY_EXIST_THAT_PRODUCT_KEY_HAD') {
            this.product = null;
            this.isAlreadyUserExist = true;
          } else if (result['message'] === 'PRODUCT_KEY_IS_NOT_EXIST') {
            this.isNotExistProductKey = true;
            this.product = null;
          } else {
            this.product = null;
          }
        });
    } else {
      alert('이용약관에 동의해 주세요!');
    }
  }

  /**
   * @description 구매증빙 이미지 업로드
   * @param {*} $event
   * @memberof RegistComponent
   */
  imageUpload($event) {
    this.isImageUploading = true;
    this.fileUploadService.uploadFile(apiURL + '/valid/receipt', $event.target.files)
      .then(
      result => {
        this.isImageUploading = false;
        this.productRegist.patchValue({ imageFileName: result['response'] });
      });
  }

  /**
   * @description 정품등록
   * @memberof RegistComponent
   */
  doSubmit() {
    if (this.productRegist.valid) {
      this.cs.loadingSpinner = true;
      this.registService.postRegistInfo(this.productRegist.value)
        .subscribe(result => {
          if (result['message'] === 'SUCCESS') {
            this.router.navigateByUrl('/result');
          }
          this.cs.loadingSpinner = false;
        }, error => {
          alert('알 수 없는 오류로 인해 정상적으로 접수가 되지 않았습니다.\r\n070-7814-0007로 연락주시기 바랍니다.');
          this.cs.loadingSpinner = false;
        }, () => {
          this.cs.loadingSpinner = false;
        });
    } else {
      alert('빈 양식이 있습니다. 다시 한 번 확인해주세요.');
    }
  }

  /**
   * @description 구매증빙 업로드 예시 노출
   * @param {*} $event
   * @memberof RegistComponent
   */
  showReceiptUploadGuide($event) {
    this.receiptGuidance.nativeElement.style.display = 'inline-block';
  }

  /**
   * @description 구매증빙 업로드 예시 숨김
   * @param {*} $event
   * @memberof RegistComponent
   */
  @HostListener('click', ['$event'])
  hideReceiptUploadGuide($event) {
    if (this.receiptGuidance.nativeElement.style.display === 'inline-block') {
      this.receiptGuidance.nativeElement.style.display = 'none';
    }
  }

}
