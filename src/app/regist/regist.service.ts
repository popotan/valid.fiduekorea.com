import { Injectable } from '@angular/core';
import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { apiURL, httpOptions } from '../../environments/environment.prod';

@Injectable()
export class RegistService {

  constructor(private $http: Http) { }

  getKeyStartswitchList() {
    return this.$http.get(apiURL + '/product/key_startswitch', httpOptions)
      .map((res: Response) => res.json());
  }
  getSerialInfo(serial) {
    return this.$http.get(apiURL + '/product/serial/' + serial, httpOptions)
      .map((res: Response) => res.json());
  }
  postRegistInfo(info) {
    return this.$http.post(apiURL + '/product/regist', info, httpOptions)
      .map((res: Response) => res.json());
  }
}
