import { Component, OnInit, AfterViewInit, Input, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-image-slider',
  templateUrl: './image-slider.component.html',
  styleUrls: ['./image-slider.component.css']
})
export class ImageSliderComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(){
    // console.log(this.image_list.nativeElement.children);
  }

  // @Input('target') target;
  // @ViewChild('image-list') image_list;
}
