import { TestBed, inject } from '@angular/core/testing';

import { ComponentStateService } from './component-state.service';

describe('ComponentStateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ComponentStateService]
    });
  });

  it('should ...', inject([ComponentStateService], (service: ComponentStateService) => {
    expect(service).toBeTruthy();
  }));
});
