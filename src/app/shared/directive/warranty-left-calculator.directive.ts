import { Directive, Input, ElementRef, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[WarrantyLeftCalculator]'
})
export class WarrantyLeftCalculatorDirective implements AfterViewInit{

  constructor(public el: ElementRef) { }

  ngAfterViewInit(){
    this.el.nativeElement.innerHTML = this.doCalculate() + '일';
  }

  @Input('start-date-object') start_date_object;
  @Input('warranty-length') warranty_length;

  doCalculate(){
    let now = new Date();
    let one_day = 1000*60*60*24;
    let edate = new Date(
      this.start_date_object.year, 
      this.start_date_object.month - 1, 
      this.start_date_object.day + this.warranty_length);
    let marginal_seconds = edate.getTime() - now.getTime();
    return Math.floor(marginal_seconds / one_day);
  }
}
