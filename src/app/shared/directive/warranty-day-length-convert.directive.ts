import { Directive, Input, ElementRef, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[WarrantyDayLengthConvert]'
})
export class WarrantyDayLengthConvertDirective implements AfterViewInit {

  constructor(public el:ElementRef) { }

  ngAfterViewInit(){
    this.el.nativeElement.innerHTML = this.warranty_length + '일 ' + '(' + this.doConvert() + ')';
  }
  @Input('warranty-length') warranty_length:number;

  doConvert(){
    switch(this.warranty_length){
      case 365:
        return '1년';
      default:
        return Math.floor(this.warranty_length/30).toString() + '개월';
    }
  }
}
