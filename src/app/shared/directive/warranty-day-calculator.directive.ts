import { Directive, Input, ElementRef, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[WarrantyDayCalculator]'
})
export class WarrantyDayCalculatorDirective implements AfterViewInit{

  constructor(public el : ElementRef) { }

  ngAfterViewInit(){
    let date = this.doCalculate();
    this.el.nativeElement.innerHTML = date.getFullYear() + '년 ' + (date.getMonth() + 1) + '월 ' + date.getDate() + '일';
  }

  @Input('start-date-object') start_date_object;
  @Input('warranty-length') warranty_length;

  doCalculate(){
    return new Date(
      this.start_date_object.year, 
      this.start_date_object.month - 1, 
      this.start_date_object.day + this.warranty_length);
  }
}
