import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingSpinnerComponent } from './component/loading-spinner/loading-spinner.component';
import { ImageSliderComponent } from './component/image-slider/image-slider.component';
import { WarrantyDayLengthConvertDirective } from './directive/warranty-day-length-convert.directive';
import { WarrantyDayCalculatorDirective } from './directive/warranty-day-calculator.directive';
import { WarrantyLeftCalculatorDirective } from './directive/warranty-left-calculator.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [LoadingSpinnerComponent, ImageSliderComponent, WarrantyDayLengthConvertDirective, WarrantyDayCalculatorDirective, WarrantyLeftCalculatorDirective],
  exports : [LoadingSpinnerComponent, ImageSliderComponent, WarrantyDayLengthConvertDirective, WarrantyDayCalculatorDirective, WarrantyLeftCalculatorDirective]
})
export class SharedModule { }
