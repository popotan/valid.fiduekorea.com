import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AscertainComponent } from './ascertain.component';

describe('AscertainComponent', () => {
  let component: AscertainComponent;
  let fixture: ComponentFixture<AscertainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AscertainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AscertainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
