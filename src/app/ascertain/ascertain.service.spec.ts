import { TestBed, inject } from '@angular/core/testing';

import { AscertainService } from './ascertain.service';

describe('AscertainService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AscertainService]
    });
  });

  it('should ...', inject([AscertainService], (service: AscertainService) => {
    expect(service).toBeTruthy();
  }));
});
