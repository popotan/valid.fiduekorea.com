import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AscertainService } from './ascertain.service';

@Component({
  selector: 'app-ascertain',
  templateUrl: './ascertain.component.html',
  styleUrls: ['./ascertain.component.css'],
  providers: [AscertainService]
})
export class AscertainComponent implements OnInit {

  /**
   * @description 정품등록 확인 결과 데이터 오브젝트, AscertainResultComponent에 target으로 전파된다.
   * @memberof AscertainComponent
   */
  result = null;

  /**
   * @description 확인하고자 하는 제품 정보(시리얼)
   * @memberof AscertainComponent
   */
  product = new FormGroup({
    serial: new FormControl('',
      Validators.compose([Validators.required]))
  });

  /**
   * @description 정품등록 확인 통신 상태
   * @memberof AscertainComponent
   */
  isValidating = false;

  /**
   * @description 정품등록 확인 실패시, 오류메세지
   * @memberof AscertainComponent
   */
  validateResultMsg = '';

  /**
   * @description 정품등록 확인 통신 후 상태
   * @memberof AscertainComponent
   */
  isAfterValidate = false;

  constructor(
    private router: Router,
    private ascertainService: AscertainService
  ) { }
  ngOnInit() {
  }

  /**
   * @description 정품등록 확인(시리얼을 통해 확인)
   * @param {*} $event
   * @memberof AscertainComponent
   */
  getProductInfo($event) {
    this.isValidating = true;
    this.ascertainService.getSerialInfo(this.product.get('serial').value)
      .subscribe(result => {
        this.isValidating = false;
        this.isAfterValidate = true;
        if (result['message'] === 'SUCCESS') {
          this.result = result['response'];
        } else if (result['message'] === 'SERIAL_OWNER_IS_NOT_EXIST') {
          this.validateResultMsg = '정품등록을 하지 않았습니다. 정품등록을 해 주시기 바랍니다!';
        } else if (result['message'] === 'PRODUCT_KEY_IS_NOT_EXIST') {
          this.validateResultMsg = '올바르지 않은 제품키 입니다. 다시 입력해 주시기 바랍니다.';
        } else {
          this.validateResultMsg = '알 수 없는 오류가 발생하였습니다. 다시 시도해 주시기 바랍니다.';
        }
      }, error => {
        this.isValidating = false;
        this.isAfterValidate = true;
        this.validateResultMsg = '알 수 없는 오류가 발생하였습니다. 다시 시도해 주시기 바랍니다.';
      }, () => {
        this.isValidating = false;
        this.isAfterValidate = true;
      });
  }
}
