import { Component, OnInit, Input } from '@angular/core';
import { FileUploadService } from '../../shared/service/file-upload.service';
import { AscertainService } from '../ascertain.service';

import { apiURL } from '../../../environments/environment.prod';

@Component({
  selector: 'app-ascertain-result',
  templateUrl: './ascertain-result.component.html',
  styleUrls: ['./ascertain-result.component.css'],
  providers: [FileUploadService, AscertainService]
})
export class AscertainResultComponent implements OnInit {
  /**
   * @description 정품인증기록 확인을 위한 데이터 오브젝트
   * @memberof AscertainResultComponent
   */
  @Input('target') target;

  /**
   * @description 이미지(구입내역) 업로드 상태
   * @memberof AscertainResultComponent
   */
  isImageUploading = false;

  constructor(
    private fileUploadService: FileUploadService,
    private ascertainService: AscertainService
  ) { }

  ngOnInit() {
  }

  /**
   * @description 구매증빙 이미지 업로드
   * @param {*} $event
   * @memberof AscertainResultComponent
   */
  upload_receipt_image_file($event) {
    this.isImageUploading = true;
    this.fileUploadService.uploadFile(apiURL + '/valid/receipt', $event.target.file)
      .then(result => {
        this.mod_receipt_image_filename(result);
      }, error => {
        alert('파일 업로드에 실패하였습니다. 다시 시도해 주세요.');
        this.isImageUploading = false;
      });
  }

  /**
   * @description 구매증빙 이미지 업로드 후 획득한 이미지명 db에 전송
   * @param {string} filename
   * @memberof AscertainResultComponent
   */
  mod_receipt_image_filename(filename: string) {
    this.ascertainService.postImageFilename(this.target, filename)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.target = result['response'];
          alert('구매기록 재등록에 성공하였습니다.');
          this.isImageUploading = false;
        } else if (result['message'] === 'MODIFY_IS_NOT_AVAILABLE') {
          alert('수정가능한 상태가 아닙니다. 고객센터로 문의해주세요.');
          this.isImageUploading = false;
        } else if (result['message'] === 'SERIAL_INFO_IS_NOT_EXIST') {
          alert('존재하지 않는 정보로 접근하였습니다. 새로고침 후, 다시 시도해 주시기 바랍니다.');
          this.isImageUploading = false;
        } else {
          alert('구매기록 재등록에 실패하였습니다. 다시 시도해 주세요!');
          this.isImageUploading = false;
        }
      }, error => {
        alert('통신에 오류가 발생하였습니다.');
        this.isImageUploading = false;
      }, () => {
        this.isImageUploading = false;
      });
  }
}
