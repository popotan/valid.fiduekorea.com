import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AscertainResultComponent } from './ascertain-result.component';

describe('AscertainResultComponent', () => {
  let component: AscertainResultComponent;
  let fixture: ComponentFixture<AscertainResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AscertainResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AscertainResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
