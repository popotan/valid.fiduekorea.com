import { Injectable } from '@angular/core';
import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { apiURL, httpOptions } from '../../environments/environment.prod';

@Injectable()
export class AscertainService {

  constructor(private $http: Http) { }

  getSerialInfo(serial) {
    return this.$http.get(apiURL + '/valid/serial/' + serial, httpOptions)
      .map((res: Response) => res.json());
  }

  postImageFilename(serial, new_filename) {
    return this.$http.post(apiURL + '/valid/serial/' + serial + '/mod/image_filename', { new_filename: new_filename }, httpOptions)
      .map((res: Response) => res.json());
  }
}
