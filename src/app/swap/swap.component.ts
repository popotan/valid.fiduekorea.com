import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ComponentStateService } from '../shared/service/component-state.service';
import { SwapService } from './swap.service';

@Component({
  selector: 'app-swap',
  templateUrl: './swap.component.html',
  styleUrls: ['./swap.component.css'],
  providers: [SwapService]
})
export class SwapComponent implements OnInit {

  /**
   * @description 기존제품 정보
   * @memberof SwapComponent
   */
  product = null;

  /**
   * @deprecated
   * @memberof SwapComponent
   */
  swap_target_list = [];

  /**
   * @description 오류메세지 노출여부
   * @memberof SwapComponent
   */
  message = {
    isKeyStartswitchNotMatched: false,
    isAlreadyUserExist: false,
    isNotExistProductKey: false
  };

  /**
   * @description 시리얼 허용 문자열(startswitch) 리스트, onInit시, 서버로부터 갱신받아온다.
   * @memberof SwapComponent
   */
  key_startswitch_list = ['TXYA65', 'TXYA73', 'TXYA83', 'F0', 'FD00'];

  /**
   * @description 신규 제품 및 사용자정보
   * @memberof SwapComponent
   */
  swapInfo = new FormGroup({
    serial: new FormControl('',
      Validators.compose([Validators.required])),
    user_name: new FormControl('',
      Validators.compose([Validators.required])),
    user_phone: new FormControl('',
      Validators.compose([Validators.required]))
  });

  constructor(
    private cs: ComponentStateService,
    private swapService: SwapService,
    private router: Router
  ) { }

  /**
   * @description 시리얼 규칙(startswitch) 획득
   * @memberof SwapComponent
   */
  ngOnInit() {
    this.swapService.getKeyStartswitchList()
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.key_startswitch_list = result['response'];
        }
      });
  }

  /**
   * @description 시리얼 검증
   * @param {*} $event
   * @memberof SwapComponent
   */
  validateSerial($event) {
    $event.preventDefault();
    this.message.isAlreadyUserExist = false;
    this.message.isNotExistProductKey = false;
    const serial = this.swapInfo.get('serial').value;
    this.swapService.getSerialInfo(serial)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.product = result['response'];
        } else if (result['message'] === 'ALREADY_EXIST_THAT_PRODUCT_KEY_HAD') {
          this.message.isAlreadyUserExist = true;
        } else if (result['message'] === 'PRODUCT_KEY_IS_NOT_EXIST') {
          this.message.isNotExistProductKey = true;
          this.product = null;
        } else {
          this.product = null;
        }
      });
  }

  /**
   * @description 전환대상 리스트 획득
   * @memberof SwapComponent
   */
  doSubmit() {
    if (this.swapInfo.valid) {
      this.cs.loadingSpinner = true;
      this.swapService.getSwapTargetList(this.swapInfo.value)
        .subscribe(result => {
          if (result['message'] === 'SUCCESS') {
            this.swap_target_list = result['response'];
            this.cs.loadingSpinner = false;
          } else {
            this.swap_target_list = [];
            alert('사용자정보와 일치하는 정보가 없습니다. 사용자명과 연락처를 다시 한 번 확인해주세요.');
            this.cs.loadingSpinner = false;
          }
        });
    } else {
      alert('빈 양식이 있습니다. 다시 한 번 확인해주세요.');
    }
  }

  /**
   * @description 전환정보 등록
   * @param {number} buy_user_id
   * @memberof SwapComponent
   */
  swapWith(buy_user_id: number) {
    if (confirm('선택한 제품을 입력한 정보로 재등록하시겠습니까? 재등록 이후, 기존 제품에 대한 보증정보는 모두 소멸합니다.')) {
      if (this.swapInfo.valid) {
        this.cs.loadingSpinner = true;
        this.swapService.postSwapInfo(buy_user_id, this.swapInfo.value)
          .subscribe(result => {
            if (result['message'] === 'SUCCESS') {
              this.router.navigateByUrl('/');
              this.cs.loadingSpinner = false;
            } else if (result['message'] === 'USER_INFO_IS_NOT_MATCHED') {
              alert('사용자정보가 일치하지 않습니다. 사용자명과 연락처를 다시 한 번 확인해주세요.');
              this.cs.loadingSpinner = false;
            }
          });
      } else {
        alert('빈 양식이 있습니다. 다시 한 번 확인해주세요.');
      }
    }
  }
}
