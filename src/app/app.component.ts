import { Component } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';

import { ComponentStateService } from './shared/service/component-state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(
    private router: Router,
    public cs: ComponentStateService
  ) {
    this.router.events.subscribe(
      state => {
        if (state instanceof NavigationStart) {
          this.cs.loadingSpinner = true;
        } else if (state instanceof NavigationEnd) {
          this.cs.loadingSpinner = false;
        }
      }
    )
  }
}
