import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { ComponentStateService } from '../../shared/service/component-state.service';
import { ReuseService } from '../reuse.service';

@Component({
  selector: 'app-accept',
  templateUrl: './accept.component.html',
  styleUrls: ['./accept.component.css'],
  providers: [ReuseService]
})
export class AcceptComponent implements OnInit {
  accept_id: number;
  reuse_info: object;
  acceptInfo = new FormGroup({
    valid_number: new FormControl('',
      Validators.compose([Validators.required, Validators.maxLength(4)])),
    is_accepted: new FormControl(true,
      Validators.compose([Validators.required]))
  });
  is_accepted = true;
  constructor(
    private cs: ComponentStateService,
    private reuseService: ReuseService,
    private router: Router,
    private route: ActivatedRoute
  ) { }
  ngOnInit() {
    this.cs.loadingSpinner = true;
    this.route.params.subscribe(param => {
      this.accept_id = param['accept_id'];
      this.getReuseInfo(param['accept_id']);
    });
  }
  getReuseInfo(accept_id) {
    this.reuseService.getReuseInfo(accept_id)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.reuse_info = result['response'];
          this.cs.loadingSpinner = false;
        } else if (result['message'] === 'REUSE_REQUEST_IS_NOT_EXIST') {
          this.reuse_info = null;
          alert('잘못된 요청이거나 이미 만료된 요청입니다.');
          this.cs.loadingSpinner = false;
          this.router.navigateByUrl('/');
        }
      });
  }
  acceptReuseInfo() {
    this.acceptInfo.controls['is_accepted'].patchValue(this.is_accepted);
    this.cs.loadingSpinner = true;
    this.reuseService.acceptReuseInfo(this.accept_id, this.acceptInfo.value)
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          alert('이전승인이 완료되었습니다.');
          this.router.navigateByUrl('/');
        } else if (result['message'] === 'USER_INFO_IS_NOT_MATCHED') {
          alert('유저정보가 일치하지 않습니다.');
        } else if (result['message'] === 'ACCEPT_REQUEST_IS_SHUTDOWN') {
          alert('승인요청이 거절되었습니다.');
          this.router.navigateByUrl('/');
        } else if (result['message'] === 'VALID_NUMBER_IS_NOT_MATCHED') {
          alert('승인번호가 일치하지 않습니다. 다시 시도해 주시기 바랍니다.');
        } else if (result['message'] === 'IS_NOT_EXIST_REUSE_REQUEST') {
          alert('존재하지 않는 요청입니다.');
          this.router.navigateByUrl('/');
        } else {
          alert('오류가 발생하였습니다. 다시 시도해 주시기 바랍니다.');
        }
        this.cs.loadingSpinner = false;
      });
  }
}
