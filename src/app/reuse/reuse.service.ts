import { Injectable } from '@angular/core';
import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { apiURL, httpOptions } from '../../environments/environment.prod';

@Injectable()
export class ReuseService {

  constructor(private $http: Http) { }

  getKeyStartswitchList() {
    return this.$http.get(apiURL + '/product/key_startswitch', httpOptions)
      .map((res: Response) => res.json());
  }
  getSerialInfo(serial) {
    return this.$http.get(apiURL + '/reuse/target/' + serial, httpOptions)
      .map((res: Response) => res.json());
  }
  postReuseInfo(buy_user_id, info) {
    return this.$http.post(apiURL + '/reuse/request/' + buy_user_id, info, httpOptions)
      .map((res: Response) => res.json());
  }
  getReuseInfo(accept_id) {
    return this.$http.get(apiURL + '/reuse/' + accept_id, httpOptions)
      .map((res: Response) => res.json());
  }
  acceptReuseInfo(buy_user_id, info) {
    return this.$http.post(apiURL + '/reuse/accept/' + buy_user_id, info, httpOptions)
      .map((res: Response) => res.json());
  }
}
