import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ComponentStateService } from '../shared/service/component-state.service';
import { ReuseService } from './reuse.service';

@Component({
  selector: 'app-reuse',
  templateUrl: './reuse.component.html',
  styleUrls: ['./reuse.component.css'],
  providers: [ReuseService]
})
export class ReuseComponent implements OnInit {

  /**
   * @description 기존 구매자정보
   * @memberof ReuseComponent
   */
  buy_user = null;

  /**
   * @description 오류메세지 노출여부
   * @memberof ReuseComponent
   */
  message = {
    isKeyStartswitchNotMatched: false,
    isNotExistProductKeyTokener: false,
    isNotExistProductKey: false
  };

  /**
   * @description 시리얼 허용 문자열(startswitch) 리스트, onInit시, 서버로부터 갱신받아온다.
   * @memberof ReuseComponent
   */
  key_startswitch_list = ['TXYA65', 'TXYA73', 'TXYA83', 'F0', 'FD00'];

  /**
   * @description 신규 사용자 정보
   * @memberof ReuseComponent
   */
  reuseInfo = new FormGroup({
    clauseAgree: new FormControl('',
      Validators.compose([Validators.requiredTrue])),
    serial: new FormControl('',
      Validators.compose([Validators.required])),
    user_name: new FormControl('',
      Validators.compose([Validators.required])),
    user_phone: new FormControl('',
      Validators.compose([Validators.required]))
  });
  constructor(
    private cs: ComponentStateService,
    private reuseService: ReuseService,
    private router: Router
  ) { }

  /**
   * @description 시리얼 규칙(startswitch) 획득
   * @memberof ReuseComponent
   */
  ngOnInit() {
    this.reuseService.getKeyStartswitchList()
      .subscribe(result => {
        if (result['message'] === 'SUCCESS') {
          this.key_startswitch_list = result['response'];
        }
      });
  }

  /**
   * @description 시리얼 검증
   * @param {*} $event
   * @memberof ReuseComponent
   */
  validateSerial($event) {
    $event.preventDefault();
    this.message.isNotExistProductKeyTokener = false;
    this.message.isNotExistProductKey = false;
    if (this.reuseInfo.get('clauseAgree').value) {
      const serial = this.reuseInfo.get('serial').value;
      this.reuseService.getSerialInfo(serial)
        .subscribe(result => {
          if (result['message'] === 'IS_NOT_EXIST_PRODUCT_KEY_TOKENER') {
            this.buy_user = null;
            this.message.isNotExistProductKeyTokener = true;
          } else if (result['message'] === 'SUCCESS') {
            this.buy_user = result['response'];
          } else {
            this.buy_user = null;
          }
        });
    } else {
      alert('이용약관에 동의해 주세요!');
    }
  }

  /**
   * @description 전환정보 등록
   * @memberof ReuseComponent
   */
  doSubmit() {
    if (this.reuseInfo.valid) {
      this.cs.loadingSpinner = true;
      this.reuseService.postReuseInfo(this.buy_user.id, this.reuseInfo.value)
        .subscribe(result => {
          if (result['message'] === 'SUCCESS') {
            alert('소유자 정보 재등록이 완료되었습니다. 기존 사용자로부터 승인요청을 기다려주시기 바랍니다.');
            this.router.navigateByUrl('/');
          } else if (result['message'] === 'ALREADY_REQUESTED') {
            alert('이미 요청된 CASE ID입니다. 기존 사용자로부터 승인요청을 기다려주시기 바랍니다.');
            this.router.navigateByUrl('/');
          }
          this.cs.loadingSpinner = false;
        }, error => {
          alert('알 수 없는 오류로 인해 정상적으로 접수가 되지 않았습니다.\r\n070-7814-0007로 연락주시기 바랍니다.');
          this.cs.loadingSpinner = false;
        }, () => {
          this.cs.loadingSpinner = false;
        });
    } else {
      alert('빈 양식이 있습니다. 다시 한 번 확인해주세요.');
    }
  }
}
