import { Valid.Fiduekorea.ComPage } from './app.po';

describe('valid.fiduekorea.com App', () => {
  let page: Valid.Fiduekorea.ComPage;

  beforeEach(() => {
    page = new Valid.Fiduekorea.ComPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
